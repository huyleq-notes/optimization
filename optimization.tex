\documentclass[12pt]{article}

\usepackage{graphicx}
%\graphicspath{ {./figures/} }
\usepackage{caption}
\usepackage{subcaption}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[margin=1in]{geometry}
\usepackage{textcomp}
\usepackage{nicematrix}
\usepackage{enumerate}
\usepackage{tcolorbox}

%\renewcommand{\arraystretch}{2}

\newcommand{\E}{\mathcal{E}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\D}{\mathcal{D}}
\newcommand{\La}{\mathcal{L}}
\newcommand{\cc}{\mathcal{C}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\CZ}{C(\R/\Z;\C)}
\newcommand{\PZ}{P(\R/\Z;\C)}
\newcommand{\Rk}{\mathbb{R}^k}
\newcommand{\Rn}{\mathbb{R}^n}
\newcommand{\Rmn}{\mathbb{R}^{m \times n}}
\newcommand{\Rnm}{\mathbb{R}^{n \times m}}
\newcommand{\Rnp}{\mathbb{R}^{n \times p}}
\newcommand{\Rm}{\mathbb{R}^m}
\newcommand{\Rp}{\mathbb{R}^p}
\newcommand{\Int}{\mathrm{int}}
\newcommand{\Ext}{\mathrm{ext}}
\newcommand{\Lainf}{L^{\infty}}
\newcommand{\linf}{l^{\infty}}
\DeclareMathOperator{\Vol}{vol}
\DeclareMathOperator{\diam}{diam}
\DeclareMathOperator{\Null}{Null}

\title{Optimization}
\author{Huy Le}

\begin{document}

\maketitle
This is a personal study of \cite{Nocedal}.

\newpage

\section{Theory of constrained optimization}
A general formulation for the problem of minimizing functions subject to constraints on the variables is
\begin{equation}
\min_{x\in\Rn}f(x)\quad \textrm{subject to}
\begin{cases}
c_i(x)=0,\quad i\in\E,\\
c_i(x)\ge0,\quad i\in\I,
\end{cases}
\label{eq:generalconstrainedproblem}
\end{equation}
where $f,c_i$ are all smooth and real-valued functions on a subsets of $\Rn$ and $\E,\I$ are two finite sets of indices.
\par
Define the \textit{feasible set} $\Omega$ to be the set of points that satisfy the constraints; that is
\begin{equation}
\Omega=\{x|c_i(x)=0,\quad i\in\E;\quad c_i(x)\ge 0,\quad i\in\I\}.
\end{equation}
In this chapter, we discuss two types of optimality conditions. \textit{Necessary conditions} are conditions that must be satisfied by any solution point (under certain assumptions). \textit{Sufficient conditions} are those that, if satisfied at a certain point $x^*$, guarantee that $x^*$ is in fact a solution.
\par
For the unconstrained optimization problem, the optimality conditions are as follows
\begin{enumerate} [(a)]
\item Necessary conditions: Local unconstrained minimizers have $\nabla f(x^*)=0$ and $\nabla^2 f(x^*)$ positive semidefinite.
\item Sufficient conditions: Any point $x^*$ at which $\nabla f(x^*)=0$ and $\nabla^2 f(x^*)$ is positive definite is a strong local minimizer of $f$.
\end{enumerate}

\subsection{Smoothness}
Smoothness of the objective functions and constraints is an important issue in characterizing the solutions. It ensures the objective functions and constraints behave in a reasonably predictable way and therefore allows algorithms to make good choices for search directions.
\par
The feasible regions usually have kinks and sharp edges, it does not mean that the constraint functions are nonsmooth. Nonsmooth boundaries can often be described by a collection of of smooth constraint functions.
\par
Nonsmooth unconstrained optimization problems can sometimes be reformulated as smooth constrained problems. An example is the unconstrained minimization of a function
\begin{equation}
f(x)=\max(x^2, x),
\end{equation}
which has kinks at $x=0$ and $x=1$ and the solution at $x^*=0$. We obtain a smooth constrained formulation of the problem by adding an artificial variable $t$ and writing
\begin{equation}
\min t \quad \textrm{s.t.} \quad t\ge x,\quad t\ge x^2.
\end{equation}
\textbf{Definition:} The active set $\A(x)$ at any feasible point $x$ consists of the equality constraint indices from $\E$ together with the indices of the inequality constraints $i$ for which $c_i(x)=0$; that is
\begin{equation}
\A(x)=\E\cup \{i\in\I | c_i(x)=0\}.
\end{equation}
At a feasible point $x$, the inequality constraint $i\in\I$ is said to be \textit{active} if $c_i(x)=0$ and \textit{inactive} if the strict inequality $c_i(x)>0$ is satisfied.

\subsection{Tangent cone and constraint qualifications}
We can determine whether or not it is possbile to take a feasible descent step away from a given feasible point $x$ by examining the first derivatives of $f$ and the constraint functions $c_i$. We can use the first order Taylor series expansion to form an approximate problem in which the objective function and constraints are all linear. This approach makes sense, however, only when the linearized approximation captures the essential geometric features of the feasible sets near $x$. If, near $x$, the linearization is fundamentally different from the feasible sets, then we cannot expect the linear approximation to yield useful information about the original problem. Hence, we need to make assumptions about the nature of the constraints $c_i$ that are active at $x$ to ensure that the linearized approximation is similar to the feasible set near $x$. Constraint qualifications are assumptions that ensure the similarity of the constraint set $\Omega$ and its linearized approximation in the neighborhood of $x$.
\par
Given a feasible point $x$, we call $\{z_k\}$ a \textit{feasible sequence approaching} $x$ if $z_k\in\Omega$ for all $k$ sufficiently large, and $z_k\to x$.
\par
We characterize a local solution of \eqref{eq:generalconstrainedproblem} as a point $x$ at which all feasible sequences approaching $x$ have the property that $f(z_k)\ge f(x)$ for all $k$ sufficiently large.
\par
A tangent is a limiting direction of a feasible sequence.
\par
\textbf{Definition:} A vector $d$ is said to be a tangent to $\Omega$ at a point $x$ if there are a feasible sequence $\{z_k\}$ approaching $x$ and a sequence of positive scalars $\{t_k\}$ with $t_k\to 0$ such that
\begin{equation}
\lim_{k\to\infty}\frac{z_k-x}{t_k}=d.
\end{equation}
The set of all tangents to $\Omega$ at $x$ is called the tangent cone and is denoted by $T_{\Omega}(x)$.
\par
We now define the set of linearized feasible directions.
\par
\textbf{Definition:} Given a feasible point $x$ and the active constraint set $\A(x)$, the set of linearized feasible directions $\F(x)$
\begin{equation}
\F(x)=\left\{d|
\begin{array}{l}
d^T\nabla c_i(x) = 0,\quad \textrm{for all } i\in\E,\\
d^T\nabla c_i(x) \ge 0,\quad \textrm{for all } i\in\A\cap \I
\end{array}
\right\}.
\end{equation}
It is important to note that the definition of the tangent cone does not rely on the algebraic specification of the set $\Omega$, only on its gemetry. The linearized feasible direction set does, however, depend on the definition of the constraint function $c_i$.
\par
Constraint qualifications are conditions under which the linearized feasible direction set $\F(x)$ is similar to the tangent cone $T_{\Omega}(x)$. In fact, most constraint qualifications ensure that these two sets are identical. These conditions ensure that $\F(x)$, which is constructed by linearizing the algebraic descriptions of $\Omega$ at $x$, captures the essential geometric features of $\Omega$, which is represented by $T_{\Omega}(x)$.
\par
\textbf{Definition:} (LICQ) Given the point $x$ and the active set $\A(x)$, we say that the linear independence constraint qualification (LICQ) holds if the set of active constraint gradients, $\{\nabla c_i(x),i\in\A(x)\}$ is linearly independent.
\par
In general, if LICQ holds, none of the constraint gradients can be zero.

\subsection{First-order optimality conditions}
We define the Lagrangian function to the general problem \eqref{eq:generalconstrainedproblem} as
\begin{equation}
\La(x,\lambda)=f(x)-\sum_{i\in\E\cup\I}\lambda_i c_i(x)
\end{equation}
\textbf{Definition:} (First-Order Necessary Conditions) Suppose that $x^*$ is a local solution of \eqref{eq:generalconstrainedproblem}, that the functions $f$ and $c_i$ are continuously differentiable, and that LICQ holds at $x^*$. Then there is a Lagrange multiplier vector $\lambda^*$, with components $\lambda^*_i,i\in\E\cup\I$, such that the following conditions are satisfied at $(x^*,\lambda^*)$
\begin{subequations}
\begin{align}
\nabla_x\La(x^*,\lambda^*)&=0,\label{eq:gradientLagrangecondition}\\
c_i(x^*)&=0\quad \textrm{for all }i\in\E,\\
c_i(x^*)&\ge 0\quad \textrm{for all }i\in\I,\\
\lambda_i^*&\ge 0\quad \textrm{for all }i\in\I,\\
\lambda_i^*c_i(x^*)&= 0\quad \textrm{for all }i\in\E\cup\I.\label{eq:complementaryconditions}
\end{align}
\label{eq:KKTconditions}
\end{subequations}
The conditions \eqref{eq:KKTconditions} are often known as the Karush-Kuhn-Tucker conditions or KKT conditions. The conditions \eqref{eq:complementaryconditions} are complementary conditions; they imply that either the constraint $c_i$ is active or $\lambda^*_i=0$, or possibly both. In particular, the Lagrange multipliers corresponding to inactive inequality constraints are zero, we can omit the terms for indices $i\notin\A(x^*)$ and rewrite condition \eqref{eq:gradientLagrangecondition} as
\begin{equation}
\nabla_x\La(x^*,\lambda^*)=\nabla f(x^*)-\sum_{i\in\A(x^*)}\lambda^*_i\nabla c_i(x^*)=0.
\end{equation}
A specical case of complementary is important and deserves its own definition.
\par
\textbf{Definition:} (Strict complementary) Given a local solution $x^*$ to \eqref{eq:generalconstrainedproblem} and a vector $\lambda^*$ satisfying \eqref{eq:KKTconditions}, we say that the strict complementary conditions hold if exactly one of $\lambda^*_i$ and $c_i(x^*)$ is zero for each index $i\in\I$. In other words, we have $\lambda^*_i>0$ for each $\i\in\I\cup\A(x^*)$.
\par
Satisfaction fo the strict complementary property usually makes it easier for algorithms to determine the active set $\A(x^*)$ and converge rapidly to the solution $x^*$.
\par
For a given problem \eqref{eq:generalconstrainedproblem} and solution $x^*$, there may be many vectors $\lambda^*$ for which the KKT conditions are satisfied. When the LICQ holds, however, the optimal $\lambda^*$ is unique.
\par
We use $A(x^*)$ to denote the matrix whose rows are the active constraint gradients at the optimal point
\begin{equation}
A(x^*)^T=[\nabla c_i(x^*)]_{i\in\A(x^*)}.
\end{equation}
The following result uses LICQ to relate the tangent cone to the set of first-order feasible directions.
\par
\textbf{Lemma:} Let $x$ be a feasible point. The following two statements are true.
\begin{enumerate}[(i)]
\item $T_{\Omega}(x)\subset\F(x)$.
\item If LICQ holds at $x$, then $T_{\Omega}(x)=\F(x)$.
\end{enumerate}
As mentioned above, a local solution of \eqref{eq:generalconstrainedproblem} is a point $x$ at which all feasible sequences $\{z_k\}$ have the property that $f(z_k)\ge f(x)$ for sufficiently large $k$. The following result shows that if such sequence exitst, then its limiting direction must make a nonnegative inner product with the objective function gradient.
\par
\textbf{Theorem:} If $x^*$ is a local solution of \eqref{eq:generalconstrainedproblem}, then we have
\begin{equation}
\nabla f(x^*)^Td\ge 0,\quad \textrm{for all } d\in T_{\Omega}(x^*).
\end{equation}
The converse of this result is not necessarily true.
\par
\textbf{Lemma:} (Farkas) Let the cone $K$ be defined as
\begin{equation}
K=\{By+Cw|y\ge 0\},
\end{equation}
where $B\in\Rnm,C\in\Rnp$ and $y,w$ are vectors of appropriate dimensions. Given any $g\in\Rn$, we have either that $g\in K$ or that there exists $d\in\Rn$ such that
\begin{equation}
g^Td<0,\quad B^Td\ge 0,\quad C^Td=0,
\end{equation}
but \textbf{not} both. If the later case occurs, the vector $d$ defines a \textit{separating plane} between $K$ and $g$.
\par
By applying this theorem to the cone $N$ defined by
\begin{equation}
N=\left\{\sum_{i\in\A(x^*)}\lambda_i\nabla c_i(x^*),\quad \lambda_i\ge 0 \textrm{for } i\in\A(x^*)\cap\I\right\}
\label{eq:conicactiveconstraintgradients}
\end{equation}
and setting $g=\nabla f(x^*)$, we have that either
\begin{equation}
g=\sum_{i\in\A(x^*)}\lambda_i\nabla c_i(x^*)=A(x^*)^T\lambda^*,\quad \lambda_i\ge 0 \textrm{ for } i\in\A(x^*)\cap\I
\end{equation}
or else there is a direction $d\in\F(x^*)$ such that $\nabla f(x^*)^Td<0$.

\subsection{Second-order conditions}
The first-order conditions, the KKT conditions, tell us how the objective function gradient and the constraints are related at a solution $x^*$. When these conditions are satisfied, a move along any $w\in\F(x^*)$ either increases the first-order approximation to the objective function (that is, $w^T\nabla f(x^*)>0$), or else keeps this value the same (that is, $w^T\nabla f(x^*)=0$).
\par
The second derivatives of the objective function and constraints concern the curvature of the Lagrangian function in the ``undecided" directions, that is $w\in\F(x^*)$ such that $w^T\nabla f(x^*)=0$, to resolve the issue of increase or decrease of $f$. For this purpose, $f,c_i$ are twice continuously differentiable.
\par
Given the linearized feasible direction set $\F(x^*)$ and some Lagrange multiplier vertoc $\lambda^*$ that satisfies the KKT conditions, we define the critical cone $\cc(x^*, \lambda^*)$ as follows
\begin{equation}
\cc(x^*,\lambda^*) = \{w\in\F(x^*)|\nabla c_i(x^*)^Tw=0\quad\textrm{for all }i\in\A(x^*)\cap\I\quad\textrm{s.t. }\lambda^*>0\}.
\end{equation}
Equivalently,
\begin{equation}
w\in\cc(x^*,\lambda^*)\Leftrightarrow
\begin{cases}
\nabla c_i(x^*)^Tw=0\quad\textrm{for all }i\in\E,\\
\nabla c_i(x^*)^Tw=0\quad\textrm{for all }i\in\A(x^*)\cap\I\quad\textrm{s.t. }\lambda^*>0,\\
\nabla c_i(x^*)^Tw\ge0\quad\textrm{for all }i\in\A(x^*)\cap\I\quad\textrm{s.t. }\lambda^*=0.
\end{cases}
\end{equation}
The critical cone contains those directions $w$ that would tend to adhere to the active inequality constraints even when we were to make small changes to the objective function (those indices $i\in\I$ for which the Lagrange multiplier components $\lambda_i^*$ is positive) and to the equality constraints. From the above definition and the fact that $\lambda_i^*=0$ for all inactive components $i\in\I\setminus\A(x^*)$, it follows that
\begin{equation}
w\in\cc(x^*,\lambda^*)\Rightarrow \lambda_i^*\nabla c_i(x^*)^Tw=0\quad\textrm{for all } i\in\E\cup\I.
\end{equation}
Hence from the KKT condition and the definition of the Lagrangian function
\begin{equation}
w\in\cc(x^*,\lambda^*)\Rightarrow w^T\nabla f(x^*)=\sum_{i\in\E\cup\I}\lambda_i^*\nabla c_i(x^*)^Tw=0.
\end{equation}
Thus the critical cone contains directions from $\F(x^*)$ for which it is not clear from the first derivative information alone whether $f$ will increase or decrease.
\par
The first theorem defines the necessary condition involving the second derivatives: If $x^*$ is a local solution, then the Hessian of the Lagrangian has nonnegative curvature along the critical directions.
\par
\textbf{Theorem:} (Second-Order Necessary Conditions) Suppose $x^*$ is a local solution of \eqref{eq:generalconstrainedproblem} and that the LICQ condition is satisfied. Let $\lambda^*$ be the Lagrange multiplier vector for which the KKT conditions \eqref{eq:KKTconditions} are satisfied. Then
\begin{equation}
w^T\nabla^2_{xx}\La(x^*,\lambda^*)w\ge 0,\quad \textrm{for all }w\in\cc(x^*,\lambda^*).
\label{eq:secondnecessaryconditions}
\end{equation}
Sufficient conditions are conditions on $f,c_i$ that ensure $x^*$ is a local solution of \eqref{eq:generalconstrainedproblem}. The second-order conditions differ from the second-order necessary conditions in that the constraint qualification is not required and the inequality \eqref{eq:secondnecessaryconditions} is replaced with a strict inequality.
\par
\textbf{Theorem:} (Second-Order Sufficient Conditions) Suppose that for some feasible point $x^*$, there is a Lagrange multiplier vector $\lambda^*$ such that the KKT conditions \eqref{eq:KKTconditions} are satisfied. Suppose also that
\begin{equation}
w^T\nabla^2_{xx}\La(x^*,\lambda^*)w> 0,\quad \textrm{for all }w\in\cc(x^*,\lambda^*),w\neq 0.
\label{eq:secondsufficientconditions}
\end{equation}
Then $x^*$ is a strict local solution for \eqref{eq:generalconstrainedproblem}.

\subsection{Second-order conditions and projected Hessian}
The second-order conditions are sometimes stated in a form that is slightly weaker but easier to verify. This form used a two-sided projection of the Lagrangian Hessian onto subspaces that are related to $\cc(x^*,\lambda^*)$.
\par
The simplest case is obtained when the multiplier $\lambda^*$ that satisfies the KKT conditions is unique (as happens, for example, when the LICQ condition holds) \textit{and} strict complementary condition holds. In this case
\begin{equation}
\cc(x^*,\lambda^*)=\Null A(x^*).
\end{equation}
In other words, $\cc(x^*,\lambda^*)$ is the nullspace of the matrix whose rows are the active constraint gradients at $x^*$. We now can define the matrix $Z$ with full column rank whose columns span the space $\cc(x^*,\lambda^*)$
\begin{equation}
\cc(x^*,\lambda^*)=\{Zu|u\in\R^{|\A(x^*)|}\}.
\end{equation}
Hence the condition \eqref{eq:secondnecessaryconditions} can be restated as
\begin{equation}
u^TZ^T\nabla^2_{xx}\La(x^*,\lambda^*)Zu\ge 0\quad \textrm{for all }u,
\end{equation}
or more succintly,
\begin{equation}
Z^T\nabla^2_{xx}\La(x^*,\lambda^*)Z\textrm{ is positive semidefinite}.
\end{equation}
Similarly, the condition \eqref{eq:secondsufficientconditions} can be restated as
\begin{equation}
Z^T\nabla^2_{xx}\La(x^*,\lambda^*)Z\textrm{ is positive definite}.
\end{equation}
One way to compute $Z$ is to apply QR factorization to the matrix of active constraint gradients whose nullspace we seek.

\subsection{Other constraint qualifications}
One situation in which the linearized feasible direction set $\F(x^*)$ is obviously an adequate representation of the actual feasible set occurs when all the active constraints are already linear; that is
\begin{equation}
c_i(x)=a_i^Tx+b_i.
\end{equation}
\textbf{Lemma:} Suppose that at some $x$, all active constraints $c_i,i\in\A(x)$, are linear functions. Then $\F(x^*)=T_{\Omega}(x^*)$. Hence, the condition that all active constraints be linear is another possible constraint qualification. It is neither stronger or weaker than LICQ.
\par
Another useful generalization of LICQ is the Mangasarian-Fromovitz constraint qualification (MFCQ).
\newline
\textbf{Definition:} (MFCQ) We say that the Mangasarian-Fromovitz constraint qualification holds if there exists a vector $w\in\Rn$
\begin{align}
\nabla c_i(x^*)^Tw&>0,\quad\textrm{for all }i\in\A(x^*)\cap\I,\\
\nabla c_i(x^*)^Tw&=0,\quad\textrm{for all }i\in\E.
\end{align}
The MFCQ is weaker than LICQ. Note that constraint qualifications are \textit{sufficient} conditions for the linear approximation to be adequate, not necessary conditions.

\subsection{A geometric viewpoint}
We mention a first-order optimality condition that depends only on the geometry of the feasible set $\Omega$ and not on the algebraic descriptions of the constraint functions $c_i$. First we define the \textit{normal cone} to the set $\Omega$ at a feasible point $x$.
\par
\textbf{Definition:} The normal cone to the set $\Omega$ at a point $x\in\Omega$ is defined as
\begin{equation}
N_{\Omega}(x)=\{v|v^Tw\le 0\quad \textrm{for all }w\in T_{\Omega}(x)\}.
\end{equation}
Geometrically, each normal vector $v$ makes an angle of at least $\pi/2$ with every tangent vector.
\par
\textbf{Theorem:} Suppose that $x^*$ is a local minimizer of $f$ in  $\Omega$. Then
\begin{equation}
-\nabla f(x^*)\in N_{\Omega}(x^*).
\end{equation}
\textbf{Lemma:} Suppose that that LICQ holds at $x^*$. Then then normal cone $N_{\Omega}(x^*)$ is simply $-N$, where $N$ is the set defined in \eqref{eq:conicactiveconstraintgradients}.

\subsection{Lagrange multipliers and sensitivity}
The Lagrange multiplier $\lambda_i^*$ tells us something about the sensitivity of the optimal objective function $f(x^*)$ to the presence of the constraint $c_i$. $\lambda_i^*$ indicates how hard $f$ is pushing or pulling the solution $x^*$ against the particular constraint $c_i$.
\par
When we choose an inactive constraint $i\notin\A(x^*)$ such that $c_i(x^*)>0$, the solution $x^*$ and the function value $f(x^*)$ are indifferent to whether this constraint is present or not. If we perturb $c_i$ by a tiny amount, it will still be inactive and $x^*$ will still be a local solution. Since $\lambda_i^*=0$, the Lagrange multiplier indicates accurately that constraint $i$ is not significant.
\par
Suppose instead that the constraint $i$ is active and let us perturb the right hand side of this constraint a little, requiring, say, that $c_i(x^*)\ge -\varepsilon\|\nabla c_i(x^*)\|$ instead of $c_i(x^*)\ge 0$. Suppose that $\varepsilon$ is sufficiently small that the perturbed solution $x^*(\varepsilon)$ still has the same set of active constraints and that the Lagrange multipliers are not much affected by the perturbation. It can be shown that
\begin{equation}
\frac{df(x^*(\varepsilon))}{d\varepsilon}=-\lambda_i^*\|\nabla c_i(x^*)\|.
\end{equation}
This concludes that if $\lambda_i^*\|\nabla c_i(x^*)\|$ is large, the optimal value is sensitive to the placement of the constraint $c_i$, while if this quantity is small, the dependence is not too strong. If $\lambda_i^*$ is exactly zero for some active constraint $c_i$, small perturbations to $c_i$ will hardly affect the optimal opjective function; then change is zero, to first order.
\par
\textbf{Definition:} Let $x^*$ be a solution of \eqref{eq:generalconstrainedproblem} and suppose that the KKT conditions are satisfied. We say that the constraint $c_i$ is strongly active or binding if $i\in\A(x^*)$ and $\lambda_i^*>0$ for some Lagrange multiplier $\lambda^*$ satisfying \eqref{eq:KKTconditions}. We say that $c_i$ is weakly active if $i\in\A(x^*)$ and $\lambda_i^*=0$ for all $\lambda^*$ satisfying \eqref{eq:KKTconditions}.
\par
Note that the analysis above is independent of scaling of the individual constraints. If, for example, $c_i$ is replaced by $10c_i$, the optimal Lagrange multiplier $\lambda_i^*$ will be replaced by $\lambda_i^*/10$, so that $\lambda_i^*\|\nabla c_i(x^*)\|$ does not change. However, if $f$ is replaced by $10f$, the Lagrange multiplier will become $10\lambda_i^*$, i.e. the sensitivity of $f$ has increased by a factor of $10$, as expected.

\subsection{Duality}
Duality theory shows how we can construct an alternative problem from the function and data of the original problem. This alternative dual problem is related to the original problem, referred as the primal problem, in a fascinating way. In some cased, the dual problem is easier to solve than the primal problem. In other cases, the dual can be used to obtain a lower bound on the optimal value of the objective function for the primal problem.
\par
This section is restricted to the special case of \eqref{eq:generalconstrainedproblem} in which there are no equality constraints and the objective $f$ and the negatives of the constraints $-c_i$ are all convex functions
\begin{equation}
\min_{x\in\Rn} f(x) \quad \textrm{subject to } c_i(x)\ge 0,\quad i=1,2,\dots,m.
\label{eq:convexinequalityproblem}
\end{equation}
Assemble the constraints into a vector function
\begin{equation}
c(x)=(c_1(x), c_2(x),\dots,c_m(x))^T.
\end{equation}
The Lagrangian function is
\begin{equation}
\La(x,\lambda) = f(x) - \lambda^Tc(x).
\end{equation}
Define the dual objective as follows
\begin{equation}
q(\lambda) = \inf_x \La(x,\lambda).
\label{eq:dualobjective}
\end{equation}
In many problems, this infimum is $-\infty$ for some value of $\lambda$. We define the domain of $q$ as the set of $\lambda$ for which $q$ is finite
\begin{equation}
\D = \{\lambda|q(\lambda)>-\infty\}.
\end{equation}
When $f,-c_i$ are complex functions and $\lambda\ge 0$, $\La(x,\lambda)$ is also convex. In this situation, all local minimizers are global minimizers, so computation of $q(\lambda)$ become a more practical proposition.
\par
The dual problem is define as
\begin{equation}
\max_{\lambda\in\Rn} q(\lambda)\quad \textrm{subject to } \lambda \in \D,\lambda \ge 0.
\label{eq:dualproblem}
\end{equation}
\textbf{Theorem:} The function $q$ defined by \eqref{eq:dualobjective} is concave and its domain $\D$ is convex.
\par
The optimal value of the dual problem \eqref{eq:dualproblem} gives a lower bound on the optimal value of the primal problem \eqref{eq:convexinequalityproblem}, as a result of the following \textit{weak duality} theorem.
\par
\textbf{Theorem:} (Weak Duality) For any $\bar{x}$ feasible for \eqref{eq:convexinequalityproblem} and any $\bar{\lambda}\ge 0$, we have
\begin{equation}
q(\bar{\lambda})\le f(\bar{x}).
\end{equation}
The KKT conditons specialized to \eqref{eq:convexinequalityproblem} are as follows
\begin{subequations}
\begin{align}
\nabla f(\bar{x}) - \nabla c(\bar{x})\bar{\lambda} &= 0,\\
c(\bar{\lambda})&\ge 0,\\
\bar{\lambda}&\ge 0,\\
\bar{\lambda}_ic_i(\bar{x})&=0,\quad i=1,2,\dots,m,
\end{align}
\label{eq:KKTconditionsforconvexinequalityproblem}
\end{subequations}
where $\nabla c(x)\in\Rnm$ defined by $\nabla c(x)=[\nabla c_1(x),\nabla c_2(x),\dots,\nabla c_m(x)]$.
\par
The next theorem shows that the optimal Lagrange multiplier for the primal problem \eqref{eq:convexinequalityproblem} is the solution to the dual problem \eqref{eq:dualproblem}.
\par
\textbf{Theorem:} Suppose that $\bar{x}$ is a solution to \eqref{eq:convexinequalityproblem} and $f,-c_i$ are convex functions on $\Rn$ that are differentiable at $\bar{x}$. Then any $\bar{\lambda}$ for which $(\bar{x},\bar{\lambda})$ satisfy the KKT conditions \eqref{eq:KKTconditionsforconvexinequalityproblem} is a solution to \eqref{eq:dualproblem}.
\par
Note that if the functions are continously differentiable and a constraint qualification such as LICQ holds, then an optimal Lagrange multiplier is guaranteed to exist.
\par
A partial converse of the above theorem shows that solutions to the dual problems can sometimes be used to derive the solutions to the primal problems. The essential condition is strict convexity of $\La(\cdot,\lambda)$ for a vertain value $\lambda$. This condition holds if either $f$ is strictly convex or if $-c_i$ is strictly convex for some $i$ with $\lambda_i>0$.
\par
\textbf{Theorem:} Suppose that $f,-c_i$ are convex and continuously differentiable on $\Rn$. Suppose that $\bar{x}$ is a solution of \eqref{eq:convexinequalityproblem} at which LICQ holds. Suppose that $\hat{\lambda}$ sovles \eqref{eq:dualproblem} and the the infimum in $\La(x,\hat{\lambda})$ is attained at $\hat{x}$. Assume further that $\La(\cdot, \hat{\lambda})$ is a strict convex function. Then $\bar{x}=\hat{x}$ is the unique solution of \eqref{eq:convexinequalityproblem} and $f(\bar{x})=\La(\hat{x},\hat{\lambda})$.
\par
A slightly different form of duality that is convenient for computation, known as the \textit{Wolfe dual}, can be stated as follows
\begin{equation}
\max_{x,\lambda} \La(x,\lambda)\quad\textrm{subject to }\nabla_x\La(x,\lambda)=0,\quad \lambda\ge 0.
\label{eq:Wolfetduality}
\end{equation}
%\begin{tcolorbox}[colframe=red]
\textbf{Theorem:} Suppose that $f,-c_i$ are convex and continuously differentiable on $\Rn$ and that $(\bar{x},\bar{\lambda})$ is a solution pair of \eqref{eq:convexinequalityproblem} at which LICQ holds. Then $(\bar{x}, \bar{\lambda})$ solves the problem \eqref{eq:Wolfetduality}.
%\end{tcolorbox}


\newpage
\bibliographystyle{unsrt}
\bibliography{ref}

\end{document}
